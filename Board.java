import java.util.*;

public class Board{
	private Die Die1;
	private Die Die2;
	private boolean[] closedTiles;
	
	public Board(){
		this.Die1 = new Die();
		this.Die2 = new Die();
		this.closedTiles = new boolean[12];
	}
	
	public String toString(){
		String tiles = " ";
		for(int i=0; i < closedTiles.length; i++){
			if(closedTiles[i] == false){
				tiles = tiles + " " +(i + 1);
			}
			else{
				tiles = tiles + " X";
			}
		}
		return tiles;
	}
	
	public boolean playATurn(){
		boolean turn = false;
		this.Die1.roll();
		this.Die2.roll();
		System.out.println(this.Die1.toString());
		System.out.println(this.Die2.toString());
		int sum = this.Die1.getPips() + this.Die2.getPips();
		if(closedTiles[sum-1] == false){
			closedTiles[sum-1] = true;
			System.out.println("Closing tile: " + sum);
			turn = false;
		}
		else{
			System.out.println("This tile is already shut.");
			turn = true;
		}
		return turn;
	}
}