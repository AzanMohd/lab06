import java.util.*;

public class ShutTheBox{
	public static void main(String[]args){
		System.out.println("Welcome to Shut the box!");
		Board board = new Board();
		boolean gameOver = false;
		
		while(!gameOver){
			System.out.println("Player 1's turn");
			System.out.println(board.toString());
			if(board.playATurn()){
				System.out.println("Player 2 wins!");
				gameOver = true;
			}
			else{
				System.out.println("Player 2's turn");
				System.out.println(board.toString());
				if(board.playATurn()){
					System.out.println("Player 1 wins!");
					gameOver = true;
				}
			}
		}
	}
}